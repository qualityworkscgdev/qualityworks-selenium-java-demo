package com.qualityworkscg.timely.selenium_demo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TimelyLoginTests {
	
	public WebDriver driver;
	
	/*
	 * This main methods runs the automation methods
	 */
	public static void main(String[] args){
		TimelyLoginTests tests = new TimelyLoginTests();
		tests.loginToTimely();
		tests.logOutToTimely();
	}
	
	@Test(description="log in to timely")
	public void loginToTimely(){
		
		/* The WebDriver is initialized to use the Firefox Browser
		 * and then goes to the specified url.
		 */
		driver = new FirefoxDriver();
		driver.get("http://timely-qw.herokuapp.com");
		
		WebElement userTitle;
		WebDriverWait wait = new WebDriverWait(driver, 60);
		
		/* Run the login automation method */
		login();
		
		By userTitleXpath = new By.ByXPath("//div[text()='Timely Tester']");
		userTitle = wait.until(ExpectedConditions.visibilityOfElementLocated(userTitleXpath));
		
		/* This assertion verifies that the user us logged in*/
		Assert.assertTrue(userTitle.isDisplayed());
		
		/*This method kills the driver and closes the web browser*/
		driver.quit();
	}
	
	@Test(description="log out of timely")
	public void logOutToTimely(){		
		driver = new FirefoxDriver();
		driver.get("http://timely-qw.herokuapp.com");
		
		WebDriverWait wait = new WebDriverWait(driver, 60);
		
		login();
		By logoutLinkId = new By.ById("btnLogout");
		WebElement logoutLink = wait.until(ExpectedConditions.visibilityOfElementLocated(logoutLinkId));
		logoutLink.click();
		
		By confirmLogoutCss = new By.ByCssSelector("div[class='ui green ok inverted button']");
		WebElement confirmLogout = wait.until(ExpectedConditions.visibilityOfElementLocated(confirmLogoutCss));
		confirmLogout.click();
		By loginButtonId = new By.ById("loginBtn");
		
		/* To verify this test with assert that the login button is visible again */
		WebElement loginButton = wait.until(ExpectedConditions.visibilityOfElementLocated(loginButtonId));
		Assert.assertTrue(loginButton.isDisplayed());
		
		driver.quit();
	}
	
	
	
	/*
	 * The login method holds the procedure for logging into timely.
	 * 
	 * A WebDriverWait may be used for each web element to pause the automation for a
	 * a maximum of 60 seconds.This wait allows these WebElements to be 
	 * loaded before the driver tries to find them.
	 * 
	 * Without the waits the driver may look for the elements before they are
	 * loaded which would cause an error.
	 * 
	 * A WebElement may be found by waiting until a certain condition
	 * is achieved at part of the page. This part of the page may
	 * be found using a By which directs the WebDriver how to find
	 * a certain WebElement
	 * 
	 * WebElements once found by the WebDriver may have commands invoked
	 * on them such as the methods click() or sendKeys()
	 */
	public void login(){
		
		WebDriverWait wait = new WebDriverWait(driver, 60);
		By loginButtonId = new By.ById("loginBtn");
		WebElement loginButton = wait.until(ExpectedConditions.visibilityOfElementLocated(loginButtonId));
		loginButton.click();
		
		By emailFieldId = new By.ById("email");
		WebElement emailField = wait.until(ExpectedConditions.visibilityOfElementLocated(emailFieldId));
		emailField.sendKeys("timelytester@gmail.com");
		
		By passwordFieldId = new By.ById("password");
		WebElement passwordField = wait.until(ExpectedConditions.visibilityOfElementLocated(passwordFieldId));
		passwordField.sendKeys("Password123");
		
		By submitButtonCss = new By.ByCssSelector("button[type='submit");
		WebElement submitButton = wait.until(ExpectedConditions.visibilityOfElementLocated(submitButtonCss));
		submitButton.click();
	}
}
