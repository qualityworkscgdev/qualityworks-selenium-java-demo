# QualityWorksCG Selenium Java Demo

This is a simple demo of the Selenium framework using Java.

### Getting Started

Install [Java](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) and then clone this repository:

```sh 
$ git clone https://wharding_qualityworkscg@bitbucket.org/qualityworkscgdev/qualityworks-selenium-java-demo.git
```
Move into the cloned directory
```sh 
$ cd qualityworks-selenium-java-demo
```
Run the automation
```sh
$ java -jar bin/com/qualityworkscg/timely/selenium_demo/qualityworkscg-selenium-demo.jar
```

** Note: ** Ensure ** Firefox v45.0.2 ** or ** higher ** is installed.

### Useful links

####[Selenium HQ](http://www.seleniumhq.org/)